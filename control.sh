#!/bin/bash

main() {
    STOP=0
    START=0
    BACKUP=0
    RESTORE=0
    STATUS=0
    LOGS=0
    RESTORE_ARG=""
    HELP=0
    TAG=""
    REMOVE=0

    #
    # Process script arguments
    #
    while [[ $# -gt 0 ]]
    do
    key="$1"
    case $key in
        stop)
            STOP=1
            ;;
        start)
            START=1
            ;;
        restart)
            STOP=1
            START=1
            ;;
        status)
            STATUS=1
            ;;
        logs)
            LOGS=1
            ;;
        backup)
            BACKUP=1
            ;;
        restore)
            RESTORE=1
            RESTORE_ARG=$2
            shift
            ;;
        remove)
            REMOVE=1
            ;;
        -h|--help)
            HELP=1
            ;;
        *)
            echo "Unkown option: $key"
            HELP=1
            ;;
    esac
    shift # past argument or value
    done

    #
    # Execute based on mode argument
    #
    if [ ${HELP} -eq 1 ]; then
        echo ""
        echo "control.sh [start|stop|restart] [-h]"
        echo ""
        echo "  start                 Start"
        echo "  stop                  Stop"
        echo "  restart               Restart"
        echo "  status                Check status of all services"
        echo "  logs                  Tail logs of all services"
        echo "  remove                Remove all containers, volumes and networks"
        echo ""
        echo "  backup                Create a backup"
        echo "  restore               Restore a backup"
        exho "  restore ls            List available backups for restore"
        echo ""
        echo "  -h, --help    Show help"
        echo ""
        exit 0
    else
        COMPOSE_OPTS=""
        COMPOSE_DIR="clarin"
        if [ $(readlink $0) ]; then
            PROJECT_DIR=$(dirname $(readlink $0))
        else
            PROJECT_DIR=$(dirname $BASH_SOURCE)
        fi
        COMPOSE_DIR=$PROJECT_DIR/$COMPOSE_DIR

        SUPERVISORCTL_PASSWORD="thepassword"
        BACKUP_PATH="../backups"
        BACKUP_NAME="frontend"

        if [ ${STOP} -eq 1 ]; then
            (cd $COMPOSE_DIR && docker-compose -f docker-compose.yml down $COMPOSE_OPTS)
        fi

        if [ "${BACKUP}" -eq 1 ]; then
            echo "Creating backup"
            (cd $COMPOSE_DIR && docker-compose exec ${BACKUP_NAME} /backup.sh)
        fi

        if [ "${RESTORE}" -eq 1 ]; then
            #Ensure a backup or command is supplied, valid commands are: ls, latest
            if [ -z "${RESTORE_ARG}" ]; then
                echo "The name of the backup to restore must be supplied."
                echo "Available backups:"
                ls "${BACKUP_PATH}"
                echo "Aborting now"
                exit 1
            fi

            #List the available backups
            if [ "${RESTORE_ARG}" == "ls" ]; then
                echo "Available backups:"
                ls "${BACKUP_PATH}"
                exit 0
            fi

            #Find the latest backup and use that
            if [ "${RESTORE_ARG}" == "latest" ]; then
                LATEST=$(cd ${BACKUP_PATH} && ls -t | head -1)
                echo "Latest=${LATEST}"
                RESTORE_ARG=${LATEST}
            fi

            echo "Restoring (arg=${RESTORE_ARG})"
            echo "Stopping ${BACKUP_NAME}"
            (cd $COMPOSE_DIR && docker-compose exec ${BACKUP_NAME} supervisorctl -u sysops -p ${SUPERVISORCTL_PASSWORD} stop grafana-webapp)
            (cd $COMPOSE_DIR && docker-compose exec ${BACKUP_NAME} supervisorctl -u sysops -p ${SUPERVISORCTL_PASSWORD} stop graphite-webapp)
            (cd $COMPOSE_DIR && docker-compose exec ${BACKUP_NAME} supervisorctl -u sysops -p ${SUPERVISORCTL_PASSWORD} stop collectd)
            (cd $COMPOSE_DIR && docker-compose exec ${BACKUP_NAME} supervisorctl -u sysops -p ${SUPERVISORCTL_PASSWORD} stop carbon-cache)
            echo "Restoring backup"
            (cd $COMPOSE_DIR && docker-compose exec frontend /restore.sh "${RESTORE_ARG}")
            echo "Starting ${BACKUP_NAME}"
            (cd $COMPOSE_DIR && docker-compose exec ${BACKUP_NAME} supervisorctl -u sysops -p ${SUPERVISORCTL_PASSWORD} start carbon-cache)
            (cd $COMPOSE_DIR && docker-compose exec ${BACKUP_NAME} supervisorctl -u sysops -p ${SUPERVISORCTL_PASSWORD} start collectd)
            (cd $COMPOSE_DIR && docker-compose exec ${BACKUP_NAME} supervisorctl -u sysops -p ${SUPERVISORCTL_PASSWORD} start graphite-webapp)
            (cd $COMPOSE_DIR && docker-compose exec ${BACKUP_NAME} supervisorctl -u sysops -p ${SUPERVISORCTL_PASSWORD} start grafana-webapp)

            echo "Restore finished"
        fi

        if [ ${START} -eq 1 ]; then
            if [ ! -d "${BACKUP_PATH}" ]; then
                echo "Creating ${BACKUP_PATH}"
                mkdir -p "${BACKUP_PATH}"
            fi

            if [ ! -f "../.env" ]; then
                (cd $COMPOSE_DIR && cp ".env-template" "../../.env")
                GENERATE_ADMIN_PASSWORD=$(head /dev/urandom | env LC_CTYPE=C tr -dc A-Zal-z0-9 | head -c 13 ; echo '')
                sed -i.bak "s/{{GENERATE_ADMIN_PASSWORD}}/${GENERATE_ADMIN_PASSWORD}/g" ".env"
                GENERATE_SECRET_KEY=$(head /dev/urandom | env LC_CTYPE=C tr -dc A-Za-z0-9 | head -c 13 ; echo '')
                sed -i.bak "s/{{GENERATE_SECRET_KEY}}/${GENERATE_SECRET_KEY}/g" ".env"
            fi
            if [ ! -e "${COMPOSE_DIR}/.env" ]; then
                (cd $COMPOSE_DIR && ln -s "../.env" ".env")
            fi

            (cd $COMPOSE_DIR && docker-compose -f docker-compose.yml up -d)
        fi

        if [ ${STATUS} -eq 1 ]; then
             (cd $COMPOSE_DIR && docker-compose -f docker-compose.yml ps)
        fi

        if [ ${LOGS} -eq 1 ]; then
            (cd $COMPOSE_DIR && docker-compose -f docker-compose.yml logs -f)
        fi

        if [ ${REMOVE} -eq 1 ]; then
            (cd $COMPOSE_DIR && docker-compose -f docker-compose.yml down -v)
        fi
    fi
}
main "$@"; exit